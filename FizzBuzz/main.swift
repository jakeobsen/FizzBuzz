//
//  main.swift
//  FizzBuzz
//
//  Created by Morten Jakobsen on 31/07/2017.
//  Copyright © 2017 Morten Jakobsen. All rights reserved.
//
//  This code is licensed under the GPLv2 license. Use it as you wish.
//

import Foundation

// The numbers that is valid for each word
let fizz = [3]
let buzz = [5,7]

// The words we want to output
let f: String = "Fizz"
let b: String = "Buzz"

// The number to start and end the loop with
let startNumber = 1
let endNumber   = 100

// Output string
var output: String

// Check for conditions function
func checkForConditions(ValidDividers: [Int], NumberToCheck: Int) -> Bool {
    // Check if NumberToCheck modulo ValidDividers is 0
    for n in ValidDividers {
        if(NumberToCheck % n) == 0 {
            // NumberToCheck is multiple of n
            return true
        }
    } // NumberToCheck is not multiple of n
    return false;
}

// Loop
for i in startNumber...endNumber {
    // Reset output
    output = ""
    
    // Check for conditions, one line for each condition
    if(checkForConditions(ValidDividers: fizz, NumberToCheck: i)) { output = "\(f)" }
    if(checkForConditions(ValidDividers: buzz, NumberToCheck: i)) { output = "\(output)\(b)" }
    
    // If output is empty, neither Fizz or Buzz conditions was met, output i
    if(output=="") { print(i) }
    // Output string, if not empty
    else { print(output) }
}
