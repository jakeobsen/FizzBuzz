# FizzBuzz

This is my response to the FizzBuzz challenge, as proposed by [Tom Scott](https://www.tomscott.com)'s video "[FizzBuzz: One Simple Interview Question](https://www.youtube.com/watch?v=QPZ0pIK_wsc)"

Use this code as you wish.
